	<%@page import="java.io.FileInputStream" %>
	<%@page import="java.io.File" %>
	<%@page import="java.io.InputStreamReader" %>
	<%@page import="java.io.FileReader" %>
	<%@page import="java.net.URL" %>
	<%@page import="java.io.BufferedReader" %>
	<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
	<style>
.button {
    background-color: #008CBA;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
 

        #diagramContainer {
            padding: 20px;
            width:35%; height: 400px;
        
        }
        
        .item {
            font-size: 1.2em;
            font-family: sans-serif;
            color:#333;
            text-decoration: none;
            display:inline-block; 
            padding:0.5em 1.75em 0.5em 1em;
            border-radius: 0.25em;
            background:	#9ACD32;
            border:1px solid #ccc;
            border-right:0.45em solid #008000; /* blue stripe */
            position: relative;
            margin:0 0 1px; 
        }
		    
        .item1 {
            font-size: 1.2em;
            font-family: sans-serif;
            color: #333;
            text-decoration: none;
            display:inline-block; 
            padding:0.5em 1.75em 0.5em 1em;
            border-radius: 0.25em;
            background:	#64bbc5;
            border:1px solid #ccc;
            border-right:0.45em solid #191970; /* blue stripe */
            position: relative;
            margin:0 0 1px; 
        }

		  .item2 {
            font-size: 1.2em;
            font-family: sans-serif;
            color: #333;
            text-decoration: none;
            display:inline-block; 
            padding:0.5em 1.75em 0.5em 1em;
            border-radius: 0.25em;
            background:	#cf5b6f;
            border:1px solid #ccc;
            border-right:0.45em solid #191970; /* blue stripe */
            position: relative;
            margin:0 0 1px; 
        }
	#headerBar2 {
    height: 60px;
    background:	#F4A460;
    width: 100%;

}
.run{
font-size: 18px;
color:white
width: 100px;
height: 100px;

background-size: 100%;
}

.dropdown {
  position: relative;
  
}


.dropdown dd,
.dropdown dt {
  margin: 0px;
  padding: 0px;
}

.dropdown ul {
  margin: -1px 0 0 0;
}

.dropdown dd {
  position: relative;
}

.dropdown a,
.dropdown a:visited {
  color: #fff;
  text-decoration: none;
  outline: none;
  font-size: 15px;
}

.dropdown dt a {
  background-color: #4F6877;
  display: block;
  padding: 8px 20px 5px 10px;
  min-height: 25px;
  line-height: 24px;
  overflow: hidden;
  border: 0;
  width: 272px;
}

.dropdown dt a span,
.multiSel span {
  cursor: pointer;
  display: inline-block;
  padding: 0 3px 2px 0;
}

.dropdown dd ul {
  background-color: #4F6877;
  border: 0;
  color: #fff;
  display: none;
  left: 0px;
  padding: 2px 15px 2px 5px;
  position: absolute;
  top: 2px;
  width: 280px;
  list-style: none;
  height: 100px;
  overflow: auto;
}

.dropdown span.value {
  display: none;
}

.dropdown dd ul li a {
  padding: 5px;
  display: block;
}

.dropdown dd ul li a:hover {
  background-color: #fff;
}

 .topcorner{
   position:absolute;
   top:20%;
   right:0%;
  }
.green{
  background-color: #6BBE92;
  width: 272px;
  border: 0;
  padding: 10px 0;
  margin: 5px 0;
  text-align: center;
  color: #fff;
  font-weight: bold;
}


input[type=text] {
    padding: 0;
    height: 30px;
    position: relative;
    left: 0;
    outline: none;
    border: 1px solid #cdcdcd;
    border-color: rgba(0,0,0,.15);
    background-color: white;
    font-size: 16px;
}
.advancedSearchTextbox {
    width: 526px;
    margin-right: -4px;
}

#abc {
width:100%;
height:100%;
top:0;
left:0;
display:none;
position:fixed;
background-color:#313131;
overflow:auto
}

#abc1 {
width:100%;
height:100%;
top:0;
left:0;
display:none;
position:fixed;
background-color:#313131;
overflow:auto
}

#abc2 {
width:100%;
height:100%;
top:0;
left:0;
display:none;
position:fixed;
background-color:#313131;
overflow:auto
}
div#popupContact {
position:absolute;
left:50%;
top:27%;
margin-left:-202px;
font-family:'Raleway',sans-serif
}
img#close {
position:absolute;
right:-14px;
top:-14px;
cursor:pointer;
}
.multiselect {
		width: 460px;
		height: 200px;
	}
    </style>
	

	 <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
<link href="css/simple-sidebar.css" rel="stylesheet">
		
		<title>Dashboard with Off-canvas Sidebar</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/base/jquery-ui.css" rel="stylesheet">
		<link href="http://trirand.com/blog/jqgrid/themes/ui.jqgrid.css" rel="stylesheet">
			<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<!-- script references -->
		<script src="http://trirand.com/blog/jqgrid/js/jquery.jqGrid.min.js"></script>
		<script src="http://trirand.com/blog/jqgrid/js/i18n/grid.locale-en.js"></script>
		<script src="https://code.highcharts.com/highcharts.js"></script>
         <script src="https://code.highcharts.com/modules/exporting.js"></script>
		
		</head>
     <body>

    <div id="wrapper">

	
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
		
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
				 <a href="#">Machine Learning Studio</a>				
			
			     </li>
				 
				
                <li>
                    <div id="dropdown-1" class="dropdown dropdown-processed">
                   <a class="dropdown-link" href="#"><b><i>Data:</i></b></a>
				   
                    <ul style="color:white">
                    <li ><a href="#" onclick="show('data')">Date</a> </li>                  
					 <li ><a href="#" onclick="show('eventLog')">Event Log File</a> </li>
                    <li> <a href="#" onclick="show('alarmLog')">Alarm Log File</a> </li>
					<li> <a href="#" onclick="show('wdl')"> WDL File </a></li>
					 <li> <a href="#" onclick="show('edit_fields')"> Choose Channel Names </a></li>
										
                  </ul>
                  
                  </div>
                </li>
                
                <li>
                    <div id="dropdown-3" class="dropdown dropdown-processed">
                    <a class="dropdown-link" href="#"><span><b><i>Machine Learning Algorithm:</i></b></span></a>
                   
                    <ul style="color:white">
                    <li> <a href="#" onclick="show('ANN')"> K-means </a></li>
                    <li> <a href="#" onclick="show('DeepLearning')"> Gaussian Mixture </a></li>
                    <li> <a href="#" onclick="show('RandomForest')"> Bisecting k-means </a></li>
                     <li> <a href="#" onclick="show('LinearRegression')"> Linear Regression </a></li>
                      <li> <a href="#" onclick="show('SVM')"> SVM </a></li>
                    </ul>
                   
                    </div>
                </li>
                <li>
                   <div id="dropdown-4" class="dropdown dropdown-processed">
                   <a class="dropdown-link" href="#"><b><i>Visualization:</i></b></a>
                   
                   <ul style="color:white">
                   <li><a href="#" onclick="show('bar')"> Bar Charts </a></li>
                    <li><a href="#" onclick="show('pie')">Scatter Plots </a></li>
                  
                  </ul>
                
                  </div>
                </li>
                
            </ul>
        </div>
		

		
        <!-- /#sidebar-wrapper -->
<div id="headerBar2">
<div class="container">
                    
                        <input type = "button" id="menu-toggle" style ="background-color:black;color:white;position: absolute; right: 8%; top: 30px;cursor: pointer;" value = "Toggle Menu" >
						</div>	
					</div>
			</div>
				
	 <div id="1" align = "center">
	 <br>
	 <br>
	 <table id="list" align = "right"></table>
     <div id="pager"></div>
	 <br>
	 <br>
	<button class="button" id="btnClick" onclick = "spark()">Visualize Spark Output</button>
	</div>
	<div  id="2" style="display:none;" align = "center">
	<div id="container" style="min-width: 500px; height: 400px; max-width: 900px; margin: 0 auto"></div>
	<br><br>
	<input type="button" class="button" id="btnClick1"  value="Back to alarm and event Details">
    </div>
  
<script>
var val = <%= request.getAttribute("jsonObject").toString() %>;

		$(document).ready(function() {
            var grid = $("#list"),
                mydata = val;
            grid.jqGrid({
            	datatype: "local",
                data: mydata,
                colNames:['AlarmName','EventName', 'EventHandler'],
                colModel:[
                    {name:'AlarmName',index:'AlarmName', key: true, width:150},
                    {name:'EventName',index:'EventName', width:150},
                    {name:'EventHandler',index:'EventHandler', width:250}                    
                ],
                search:true,
                pager:'#pager',
                jsonReader: {cell:""},
                rowNum: 10,
                rowList: [5, 10, 20, 50],               
                viewrecords: true,
                multiSort: true, 
                multiselect: true, 

                height: "100%",
                caption: "Event and Alarm log file"
            });
            grid.jqGrid('navGrid','#pager',{add:false,edit:false,del:false,search:true,refresh:true},
                        {},{},{},{multipleSearch:true, multipleGroup:true, showQuery: true});
           
        });
		
	
function spark() {

	  <%String filePath = "/var/lib/jenkins/Desktop/logFiles/sparkOutput.txt";
	    System.out.println("herererere");
	    BufferedReader bf = new BufferedReader (new FileReader(filePath));
	    
	    String line = bf.readLine();
	    String line1 = bf.readLine();
	    String line2 = bf.readLine();
	    String line3 = bf.readLine();
	    System.out.println("line"+line+"line1"+line1+"line2"+line2+"line3"+line3);
	 
	    %>
	/*var reader = new XMLHttpRequest() || new ActiveXObject('MSXML2.XMLHTTP');
	var output1;
	function loadFile() {
	    reader.open('get', 'file/sparkOutput.txt', false); 
	    reader.onreadystatechange = displayContents;
	    output1 = displayContents; 
	    reader.send(null);
	}

	function displayContents() {
	    if(reader.readyState==4) {
	        if(reader.status === 200 || reader.status == 0) {
			
	       output1 = reader.responseText;
		  
	        }
	    }
	}
	loadFile();
	alert(output1);
	*/
	    if("<%=line %>".startsWith("regression")){
	    	alert("Regression");
	    var xCoordinates = "<%=line1%>";			
	    var yCoordinates = "<%=line3%>";
	    alert(xCoordinates);
	    alert(yCoordinates);
	    var xCoordinates1 = JSON.parse(xCoordinates);	
	    var yCoordinates1 = JSON.parse(yCoordinates);
	    alert(xCoordinates1);
	    alert(yCoordinates1);
	    	  $('#container').highcharts({
	            title: {
	                text: 'Scatter plot with regression line'
	            },
	            series: [{
	                type: 'scatter',
	                name: 'Regression Line',
	                data: xCoordinates1,
	                marker: {
	                    enabled: true
	                },
	                states: {
	                    hover: {
	                        lineWidth: 0
	                    }
	                },
	                enableMouseTracking: false
	            }, {
	                type: 'line',
	                name: 'Observations',
	                data: yCoordinates1

	            }]
	        });
	    	
	    	}else
	
	{
	var xAxis = "Temperature";
	var yAxis = "Pressure";
	
	var xCoordinates =  "<%=line%>";
					
	var yCoordinates = "<%=line2%>";
	var xCoordinates1 = JSON.parse(xCoordinates);	
	var yCoordinates1 = JSON.parse(yCoordinates);
		alert(xCoordinates1);
		alert(yCoordinates1);
	    $('#container').highcharts({
	        chart: {
	            type: 'scatter',
	            zoomType: 'xy'
	        },
	        title: {
	text: 'Spark Machine Learning Prediction'
	        },
	        
	        xAxis: {
	            title: {
	                enabled: true,
	                text: xAxis
	            },
	            startOnTick: true,
	            endOnTick: true,
	            showLastLabel: true
	        },
	        yAxis: {
	            title: {
	                text: yAxis
	            }
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'left',
	            verticalAlign: 'top',
	            x: 100,
	            y: 70,
	            floating: true,
	            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
	            borderWidth: 1
	        },
	        plotOptions: {
	 scatter: {
	                marker: {
	                    radius: 5,
	                    states: {
	                        hover: {
	                            enabled: true,
	                            lineColor: 'rgb(100,100,100)'
	                        }
	                    }
	                },
	                states: {
	                    hover: {
	                        marker: {
	                            enabled: false
	                        }
	                    }
	                },
	                tooltip: {
	                    headerFormat: '<b>{series.name}</b><br>',
	                    pointFormat: '{point.x}, {point.y}'
	                }
	            }
	        },
	        series: [{
	            name: 'Test Data',
	            color: 'rgba(223, 83, 83, .5)',
	            data:xCoordinates1
	}, {
	            name: 'Cluster Center',
	            color: 'rgba(119, 152, 191, .5)',
	            data: yCoordinates1
	        }]
	    });
	}
	};


$('#btnClick').click(function() {
    $('#1').hide();
    $('#2').show();
});
$('#btnClick1').click(function() {
    $('#2').hide();
    $('#1').show();
});

		</script>
	</body>
</html>
