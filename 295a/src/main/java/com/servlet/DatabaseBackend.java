package com.servlet;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.AsyncHandler;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * Servlet implementation class DatabaseBackend
 */

@WebServlet("/DatabaseBackend")
public class DatabaseBackend extends HttpServlet {
    private static final long serialVersionUID = 1L;
       

    public DatabaseBackend() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
               
        //Storing Parameters received from client based on user input
            String algo = request.getParameter("algo");
            System.out.println("Algorithm name "+algo);
            String channelNames = request.getParameter("channelNames");
            System.out.println("channel name "+channelNames+"channel details"+request.getParameter("channelNames"));
            String waferId = request.getParameter("waferId");
            String starttime = request.getParameter("startDate");
            String endtime = request.getParameter("endDate");

            String line = "";

            StringBuffer jsonString = new StringBuffer();


      try {

          System.out.println("call ES");

          //Elasticsearch call based on date range to retrieve event and alarm data
          URL url = new URL("http://localhost:9200/alarmeventdata/_search?"
            + "filter_path=hits.hits._source&pretty=1&size=10000000");            
          String payload = "{\"query\":"
                         + "{\"filtered\":"
                             + "{\"filter\":"
                                  + "{\"range\":"
                                     + "{\"StartDate\":"
                                        + "{\"gte\":\""+starttime+"\",\"lte\":\""+endtime+"\"}}}}},"
                         + "\"_source\":{\"include\":"
                             + "[\"AlarmName\",\"EventName\", \"EventHandler\"]}}}";
                          
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();  

        //Output file to store logging part          
        PrintWriter pw = new PrintWriter("/var/lib/jenkins/Desktop/logFiles/output.txt","UTF-8");
        
        //Connection to fetch Event and Alarm details based on date range
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        
        OutputStreamWriter writer2 = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
        writer2.write(payload);
        pw.println(payload);
    
        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
          while ((line = br.readLine()) != null) {
         jsonString.append(line);
        }
    
      response.setContentType("application/json");
   
      //Converting the Stringbuffer to json array
      JSONObject jsonObj = new JSONObject(jsonString.toString());
      JSONObject hitsObject = jsonObj.getJSONObject("hits");  
      JSONArray hitsArray = hitsObject.getJSONArray("hits");
      JSONArray array = new JSONArray();

      //filtering the _source data from Elasticsearch output
      for (int i = 0 ; i < hitsArray.length(); i++) {
        JSONObject jObject = hitsArray.getJSONObject(i);
        array.put(jObject.get("_source"));         
       } 
   
      //writing the spark logging part to the file created above.
      response.getWriter().append("Submitting to spark. algo=" + algo + " channels=" + channelNames
            + " waferId=" + waferId);
      String cmd = "/var/lib/jenkins/Desktop/puja/mllib/runspark.sh " + algo + " trainingdata testdata result " + waferId + " " + channelNames;
      Runtime run = Runtime.getRuntime();
      Process pr = run.exec(cmd);
      System.out.println("Spark completed Process");
    
        
      BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
      pw.println(response.getWriter());

      while ((line=buf.readLine())!=null) {
          //response.getWriter().append(line + "\n");
          pw.println( line);
          //response.getWriter().flush();
         
      }

      //Forwarding the response to the result.jsp file.
      request.setAttribute("jsonObject", array);
      RequestDispatcher dispatcher = request.getRequestDispatcher("result.jsp");
      dispatcher.forward(request, response);
      pw.close();
                                
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage());
    } 
  }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}